from flask import Blueprint, request, Flask
import json

api = Blueprint('api', __name__)


@api.route('/plus_one')
def plus_one():
    x = int(request.args.get('x', 1))
    return json.dumps({'x': x + 1})


@api.route('/plus_two')
def plus_two():
    x = int(request.args.get('x', 1))
    return json.dumps({'x': x + 2})


@api.route('/square')
def square():
    x = int(request.args.get('x', 1))
    return json.dumps({'x': x * x})
