import pytest


@pytest.fixture
def client():
    from main import app

    app.config['TESTING'] = True
    yield app.test_client()
